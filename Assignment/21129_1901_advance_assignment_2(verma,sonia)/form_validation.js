//****************************** JS File to Check Form Validation ******************************


function checkFormValidation(){
  // all data from form in a variable.
  var form_data = document.getElementById("my_form");
  // array to store the values
  var form_values = new Array();
  form_values[0] = document.getElementById("first_name").value;
  form_values[1] = document.getElementById("last_name").value;
  form_values[2] = document.getElementById("email_address").value;
  form_values[3] = document.getElementById("phone").value;
  form_values[4] = document.getElementById("postal_code").value;
  form_values[5] = document.getElementById("file_url").value;
  form_values[6] = document.getElementById("person_age").value;
 
  // array to store name of the form fields.
  var form_fields = new Array();
  form_fields[0] = "First Name";
  form_fields[1] = "Last Name";
  form_fields[2] = "Email";
  form_fields[3] = "Phone Number";
  form_fields[4] = "Postal Code";
  form_fields[5] = "Url";
  form_fields[6] = "Age";
 
  // loop for form_value array
  for(i=0;i<form_values.length;i++){
    //if statement to check that form values is empty or not.
    if(form_values[i] == ''){
      alert(form_fields[i] + " is empty, Please Fill " + form_fields[i] + " in the form field");
      return false;
    }//end IF
  }// loop end
  
  //check first name validation
  if(!checkFirstName()){
    return false;
  }
  
  // check last name validation 
  if(!checkLastName()){
    return false;
  }
 
  // Check email validation
  if(!checkEmail()){
    return false;
  }
 
  // Check email validation
  if(!checkPhone()){
    return false;
  }
 
  // check Postalcode
  if(!checkPostal()){
    return false;
  }
 
  // check Url Validation
  if(!checkUrl()){
    return false;
  }
  
  // check Age validation
  if(!checkAge()){
    return false;
  }
 
  form_data.submit();// this is used to submit the form 
  
  // clear the form fields so we can type new values in the form.
  var the_form = document.getElementById("my_form");
  for(i=0; i<the_form.elements.length;i++){
    if(the_form.elements[i].type != "button" && the_form.elements[i].type != "reset"){
      the_form.elements[i].value = "";
    }
  }
  // getting the form focus on first firld
  document.getElementById("first_name").focus();
}

// *********************** Functions for fifferent field with validation condition ***************************

// ********************************************************** function to check first name from the form field
function checkFirstName(){
  //1st way to check name
  var first_name_patt = /[^a-z\ ?]+/gi;
  // getting the value of first name from form field.
  var person_first_name = document.getElementById("first_name").value;
  person_first_name = person_first_name.trim();
  var result = first_name_patt.test(person_first_name);
  // if condition to check the refular expression.
  if(result){
    alert('Please enter alphabets only.');
    document.getElementById("first_name").focus();
    document.getElementById("first_name").value = "";
    return false;
  }//end if statement
  else{
    return true;
  }// end else statement
}// end of checkFirstName Function


// ************************************************************ function to check last name from the form field
function checkLastName(){
  //2nd way to check name
  var last_name_patt = /^[a-zA-Z\s?]+$/;
  // getting the value of last name from form field.
  var person_last_name = document.getElementById("last_name").value;
  person_last_name = person_last_name.trim();
  var result = last_name_patt.test(person_last_name);
  // if condition to check the refular expression.
  if(!result){
    alert('Please enter Alpahelts only.');
    document.getElementById("last_name").focus();
    document.getElementById("last_name").value = "";
    return false;
  }//end if statement
  else{
    return true;
  }// end else statement
}// end of checklastName Function


// ********************************************************* function to check email address from the form field
function checkEmail(){
  // variable declaration
  var email_add_parts;
  var email_add_parts2;
  // getting the value of email address from form field
  var email_add = document.getElementById("email_address").value;
  // regular expression for the different parts of the emil address.
  var prefix_patt = /[a-zA-Z\d\.\-\_][a-zA-Z\d]$/;
  var domain_patt = /^[a-zA-Z][a-zA-Z\d\-]/;
  var top_level_domain_patt = /([a-z]{1,})$/;
   //loop for checking "@" and "." symbol.
  while((email_add.indexOf("@") < 1) ||
        (email_add.indexOf("@") == email_add.length-1) ||
        (email_add.indexOf("@") == email_add.length-2) ||
        (email_add.indexOf(".") < 1) ||
        (email_add.indexOf(".") == email_add.length-1) ||
        (email_add.indexOf(".") == email_add.length-2) ||
        (email_add.indexOf(' ') != -1)){
    alert("Invalid Email!! Try Again.");
    document.getElementById("email_address").focus();
    document.getElementById("email_address").value="";
    return false;
  }// end loop
   
  // splitting up the input value by "@" symbol
  email_add_parts = email_add.split("@");
    if(email_add_parts.length>2){
      // split by "." symbol.
      alert("there should be one @ symbol in the eamil address.");
      document.getElementById("email_address").focus();
      document.getElementById("email_address").value="";
    }// end of split "@" if statement
    email_add_parts2 = email_add_parts[1].split(".");
    if(email_add_parts2.length>2){
      alert("Incorrect Email Address");
      document.getElementById("email_address").focus();
      document.getElementById("email_address").value="";
    }// end of split "." if statement
    else{  
      var res1 = prefix_patt.test(email_add_parts[0]);
      var res2 = domain_patt.test(email_add_parts2[0]);
      var res3 = top_level_domain_patt.test(email_add_parts2[1]);
      if(res1 == false || res2 == false || res3 == false){
        alert("Invalid Email Address ");
        document.getElementById("email_address").focus();
        document.getElementById("email_address").value="";
        return false;
      }// end IF
    return true;
  }
}//end Function


// ********************************************************* function to check Phone number from the form field
function checkPhone(){
  var phone_no_patt = /^\+?(\d{1,3})?\s?\(?\d{3}\)?\s?\-?\.?\d{3}[\-\s\.]\d{4}$/;
  // getting the value of Phone number from form field.
  var mobile_no = document.getElementById("phone").value;
  mobile_no = mobile_no.trim();
  // if condition to check the refular expression.
  if(!phone_no_patt.test(mobile_no)){
    alert("enter the right phone number");
    document.getElementById("phone").focus();
    document.getElementById("phone").value="";
    return false;
  }//end if statement
  else{
    return true;
  }// end else statement
}//end Function


// ********************************************************* function to check postal code from the form field
function checkPostal(){
  var postal_code_pattern = /^[A-Z]\d[A-Z]\s?\d[A-Z]\d$/i; 
  // getting the value of posatl code from form field.
  var Zip_codes = document.getElementById("postal_code").value;
  Zip_codes = Zip_codes.trim();
  // if condition to check the refular expression.
  if(!postal_code_pattern.test(Zip_codes)){
    alert("enter the right Zip code");
    document.getElementById("postal_code").focus();
    document.getElementById("postal_code").value="";
    return false;
  }//end if statement
  else{
    return true;
  }// end else statement
}// End Function


// ********************************************************* function to check url from the field 
function checkUrl(){
  // {1,} --> this will check for minimum 1 and more.
  var web_address_pattern = /^https?\:\/\/([w]{3}\.?)[a-zA-Z\d\-]+\.([a-z]{2,})$/;
  // getting the value of Age from form field.
  var web_address = document.getElementById("file_url").value;
  web_address = web_address.trim();
  // if condition to check the refular expression.
  if(!web_address_pattern.test(web_address)){
    alert("use pattern like 'http://domain.com'");
    document.getElementById("file_url").focus();
    document.getElementById("file_url").value="";
    return false;
  }//end if statement
  else{ 
    return true;
  }// end else statement    
}// end Funciton


// ********************************************************* function to check age validation 
function checkAge(){
  var age_patt = /^[1-9]\d?$/;// this will check nuber 0 to 99.
  // getting the value of Age from form field.
  var the_age = document.getElementById("person_age").value; 
  the_age = the_age.trim();
  // if condition to check the refular expression.
  if(!age_patt.test(the_age)){
    alert("please enter 2-digit numeric value only");
    document.getElementById("person_age").focus();
    document.getElementById("person_age").value="";
    return false;
  }//end if statement
  else{
    return true;
  }// end else statement
}//end function
