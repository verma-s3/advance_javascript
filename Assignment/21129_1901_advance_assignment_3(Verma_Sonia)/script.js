/* ****************************************** local Storage Scripting ********************************** */

//global variable declaration
var data_key;
var data_value;
var data_clear;
var site_visiting = 1;
var counter_result;


// function to save the data to local storage.
function saveData(){
  // check to see if local storage is supported.
  if(typeof(Storage) !== "undefined"){
    // set the local storage
    var my_form = document.getElementById("form_info");
    // loop to set the local storage for fi=orm elements
    for (i=0; i< my_form.elements.length; i++){
      data_key = my_form.elements[i].id;
      data_value = my_form.elements[i].value;
      //set item for local storage
      localStorage.setItem(data_key,data_value);
    }
   
    alert("information Saved");
    //loop to get the field empty
    for (i=0; i< my_form.elements.length; i++){
      my_form.elements[i].value = "";
    }
    
    //put the focus on the first firld of the form
    document.getElementById("full_name").focus();
    
    //get the current counter.
    // if statement for checking the counter_visit exits.
    if(localStorage.getItem("counter_visit")){
      //getting the current visits 
      site_visiting = localStorage.getItem("counter_visit");
      site_visiting = parseInt(site_visiting) + 1 ;
    }
    else{
      site_visiting = 1;
    }
    localStorage.setItem('counter_visit',site_visiting);
  }
  else{
    alert("local stroage is not supported in your browser...");
  } 
  // displaying counter visits at every submission.
  document.getElementById("counting").innerHTML = "Site Visits: " + localStorage.getItem("counter_visit");
}//end of saveData function

// function to retrive data from local storage
function viewData(){ 
  //alert(localStorage.getItem("counter_visit"));
  site_visiting = localStorage.getItem("counter_visit");
  document.getElementById("counting").innerHTML = "Site Visits: " + site_visiting;
    // loop to display local storage
  for(i=0;i<localStorage.length;i++){
    data_key = localStorage.key(i);
    data_value = localStorage.getItem(data_key);
  }
  // changing the background color and text color
  document.querySelector("body").style.background = localStorage.getItem("bgcolor");
  document.querySelector("body").style.color = localStorage.getItem("bgcolor");
 
}//end of viewData function

// function to clear data from local storage
function clearStorage(){
  for(i= localStorage.length-1;i>=0;i--){
    if(localStorage.key(i) != "counter_visit"){
      //to clear the local storage
      localStorage.removeItem(localStorage.key(i));  
    }
  }
  // to refresh the page  
  window.history.go(0);
}//end of clearStorage function

/* ---------------------------------------------------- canvas Animation ------------------------------------------------------ */ 

//onload function to run the animation when page loads.
window.onload = function(){
  // getting the canvas as an object
  var canvas_var = document.getElementById("canvas_box");
  canvas_var.width = 400;
  canvas_var.height = 400;
  //set interval for the animationRect function  
  setInterval(animateRect, 100);
  var r = 0;
  var i = 0;
  // get the "context" of our canvas element,this is usually "2d".
  var cont = canvas_var.getContext("2d");
  
  // function to animate the object
  function animateRect() {
    cont.fillStyle = i === 0 ? 'rgb(14, 134, 14)' : 'rgb(8, 8, 92)';
    cont.clearRect(0, 0, 600, 200);
    cont.save();
    cont.translate(200, 200);
    cont.rotate(r /Math.PI);
    cont.fillRect(0, 0, 100, 100);
    if (i === 0){
      i = 1;
    }//end if
    else{
      i = 0;
    }// end else
    cont.restore();
    r += 0.1;
  }
}//end of function

/* ------------------------------------------- drag and drop ------------------------------------------------- */

//this function is called with ondragstart event handler
function setChar(evt){
  // getting id of triggered drag event
  evt.dataTransfer.setData("Text", evt.target.id);
}//end of setChar function

// this function is called with ondragover event handler
function allowChar(evt){
  // prevent the defualt action
  evt.preventDefault();
}//end of allowChar function

// this function is called with ondrop event handler.
function getChar(evt){
  // prevent the default action
  evt.preventDefault();
  var data = evt.dataTransfer.getData("Text");
  //adding the new class to element.
  evt.target.appendChild(document.getElementById(data));
  // changing the CSS properties.
  document.getElementById(data).style.marginTop = "0";
  document.getElementById(data).style.top = "0";
  document.getElementById(data).style.left = "0";
}//end of getChar function